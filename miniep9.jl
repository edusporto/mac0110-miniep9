using Test
using LinearAlgebra

function test()
    @test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
    @test matrix_pot([1 2 ; 3 4], 2) ≈ [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot(
        [4 8 0 4;
         8 4 9 6;
         9 6 4 0;
         9 5 4 7], 7) ≈ [5.64e8 4.71e8 3.24e8 3.52e8;
                         8.31e8 6.94e8 4.77e8 5.18e8;
                         5.77e8 4.81e8 3.31e8 3.60e8;
                         7.99e8 6.67e8 4.58e8 5.00e8] atol=1e7

    @test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2) ≈ [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot_by_squaring(
        [4 8 0 4;
         8 4 9 6;
         9 6 4 0;
         9 5 4 7], 7) ≈ [5.64e8 4.71e8 3.24e8 3.52e8;
                         8.31e8 6.94e8 4.77e8 5.18e8;
                         5.77e8 4.81e8 3.31e8 3.60e8;
                         7.99e8 6.67e8 4.58e8 5.00e8] atol=1e7

    println("Tests passed!")
end

function matrix_pot(M, p)
    ret = copy(M)

    for i in 1:p-1
        ret = multiplica(ret, M)
    end

    return ret
end

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)

    if dima[2] != dimb[1]
        return -1
    end

    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end

    return c
end

function matrix_pot_by_squaring(M, p)
    p < 0  && return matrix_pot_by_squaring(1/M, -p)
    p == 0 && return Matrix(LinearAlgebra.I, size(M))
    p == 1 && return M

    # p > 1
    if p % 2 == 0
        return matrix_pot_by_squaring(multiplica(M, M), p ÷ 2)
    else
        return M * matrix_pot_by_squaring(multiplica(M, M), (p-1) ÷ 2)
    end
end

# test()

